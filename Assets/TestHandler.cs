﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class TestHandler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private static string ProductToString(Product product)
    {
        return "{ productId: " + product.definition.id + ", " +
            "transactionId: " + product.transactionID + ", " +
            "hasReceipt: " + product.hasReceipt + ", " +
            "receipt: " + product.receipt + "" +
        "}";
    }

    public static void OnPurchaseComplete(Product product)
    {
        Debug.Log($"OnPurchaseComplete {ProductToString(product)}");

    }

    public static void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
    {
        Debug.Log("Product purchase failed " + ProductToString(product));
        Debug.Log("Reason " + reason);
    }
}
