using System.IO;
using UnityEditor.Android;
using UnityEngine;

namespace PluginsRoot
{
    public class AndroidPostBuildProcessor : IPostGenerateGradleAndroidProject
    {
        public int callbackOrder => 0;

        void IPostGenerateGradleAndroidProject.OnPostGenerateGradleAndroidProject(string path)
        {
            Debug.Log("[AndroidPostBuildProcessor] fix Gradle properties");
            string gradlePropertiesFile = path.Replace("/unityLibrary", "") + "/gradle.properties";
            Debug.Log($"[AndroidPostBuildProcessor] File {gradlePropertiesFile} exists: {File.Exists(gradlePropertiesFile)}");
            var defaultProps = "";
            if (File.Exists(gradlePropertiesFile))
            {
                defaultProps = File.ReadAllText(gradlePropertiesFile);
                Debug.Log("[AndroidPostBuildProcessor] " + defaultProps);
                File.Delete(gradlePropertiesFile);
            }

            StreamWriter writer = File.CreateText(gradlePropertiesFile);
            writer.Write(defaultProps);
            writer.WriteLine("android.useAndroidX=true");
            writer.WriteLine("android.enableJetifier=true");
            writer.Flush();
            writer.Close();
        }
    }
}