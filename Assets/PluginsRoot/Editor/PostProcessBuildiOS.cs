using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

namespace PluginsRoot
{
    public static class PostProcessBuildiOS
    {
        [PostProcessBuild]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string buildPath)
        {
            if (buildTarget == BuildTarget.iOS)
            {
                var projectPath = Path.Combine(buildPath, "Unity-iPhone.xcodeproj/project.pbxproj");
                var project = new PBXProject();
                project.ReadFromString(File.ReadAllText(projectPath));

                // Add the -ObjC linker flag and frameworks
                var target = project.TargetGuidByName(PBXProject.GetUnityTargetName());
                AddFrameworks(project, target);

                // Write modified Xcode project back to original location
                File.WriteAllText(projectPath, project.WriteToString());
            }
        }
        
        static void AddFrameworks(PBXProject project, string targetGUID)
        {
            // Add `-ObjC` to "Other Linker Flags".
            project.AddBuildProperty(targetGUID, "OTHER_LDFLAGS", "-ObjC");
 
            // Add frameworks
            project.AddFrameworkToProject(targetGUID, "Security.framework", false);
            project.AddFrameworkToProject(targetGUID, "AdSupport.framework", false);
            project.AddFrameworkToProject(targetGUID, "iAd.framework", false);
        }
    }
}
