using System.IO;
using UnityEditor;
using UnityEngine;

namespace PluginsRoot.Utils
{
    public static class ScriptableObjectUtility
    {
        /// <summary>
        //	This makes it easy to create, name and place unique new ScriptableObject asset files.
        /// </summary>
        public static T CreateAsset<T>(string name) where T : ScriptableObject
        {
#if !UNITY_EDITOR
            return null;
#else
            T asset = ScriptableObject.CreateInstance<T>();
 
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (path == "") 
            {
                path = "Assets/Resources";
            } 
            else if (Path.GetExtension(path) != "") 
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }
 
            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/" + name);
 
            AssetDatabase.CreateAsset(asset, assetPathAndName);
 
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
            
            return asset;
#endif
        }
        
        public static void SaveAssets()
        {
#if UNITY_EDITOR
            AssetDatabase.SaveAssets();
#endif
        }
    }
}
