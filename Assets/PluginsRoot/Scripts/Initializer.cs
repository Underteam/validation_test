using System;
using System.Threading.Tasks;
using AppsFlyerSDK;
using GameCore.Common;
using PluginsRoot.Stats;
using PluginsRoot.Appsflyer;
using PluginsRoot.RestAPI;
using PluginsRoot.Network;
using PluginsRoot.Payment;
using PluginsRoot.Social;
using PluginsRoot.Tools;

namespace PluginsRoot
{
    public static class Initializer
    {
        public static SkytecRestAPI RestAPI;

        public static event Action<int> OnInstall;
        public static event Action OnComplete;
        
        public static async void InitializeAsync(NetSystem netSystem)
        {
            // ожидаем получения AdvertisingId
            while (AdvertisingIdHelper.AdvertisingId == null)
            {
                await Task.Delay(100);
            }
            
            PlayerPrefsStorage.Initialize();
            
            var config = PluginsConfig.Get();
            netSystem.SetBaseUrl(config.Server.GetFullUrl());
            
            RestAPI = new SkytecRestAPI(netSystem);

            int installId = PlayerPrefsStorage.InstallId;
            if (installId == -1)
            {
                var request = RestAPI.Install(new StatsInstallData(CookFormDataHelper.CookFormData("", AppsFlyerCallbackStates.NoCallback), true));
                installId = await GetInstallIdAsync(request);
            }
            OnInstall?.Invoke(installId);

            var shouldAttach = !string.IsNullOrEmpty(PlayerPrefsStorage.Token);
            await SocialAuth(true, shouldAttach);

            string token = null;
            bool tokenTestSuccess = false;
            while (!tokenTestSuccess)
            {
                token = PlayerPrefsStorage.Token;
                if (token != null)
                {
                    RestAPI.SetToken(token);
                    
                    var request = RestAPI.Test();
                    var result = await request.GetResponseAsync();
                    if (result.code.status > 0)
                    {
                        RestAPI.SetToken(null);
                        PlayerPrefsStorage.Token = null;
                    }
                    else
                    {
                        tokenTestSuccess = true;
                    }
                }
                else
                {
                    var loginData = GetLoginData(true);
                    if (loginData != null)
                    {
                        var loginRequest = RestAPI.Login(loginData);
                        token = await GetTokenAsync(loginRequest);

                        if (token == null)
                        {
                            var startRequest = RestAPI.Start(new StartData
                            {
                                build = PlayerPrefsStorage.Build,
                                language = PlayerPrefsStorage.Language,
                                start = true,
                                v = PlayerPrefsStorage.Version,
                                deviceId = PlayerPrefsStorage.DeviceId
                            });
                            token = await GetTokenAsync(startRequest);
                        }
                    }
                    RestAPI.SetToken(token);
                }
            }
            
            AppLovinController.SetUserId("" + PlayerPrefsStorage.UserId);

#if UNITY_ANDROID
            RestAPI.SetMarket(PayMarket.google);
#elif UNITY_IOS
            RestAPI.SetMarket(PayMarket.apple);
#endif            
            OnComplete?.Invoke();
            PaymentProcess.Initialize(RestAPI);

#if UNITY_EDITOR
            RestAPI.Reg(new StatsRegData(CookFormDataHelper.CookFormData("", AppsFlyerCallbackStates.NoCallback)));
#else
            while (AppsFlyerData.State == AppsFlyerCallbackStates.NoCallback)
            {
                await Task.Delay(100);
            }
            RestAPI.Reg(new StatsRegData(AppsFlyerData.Data));
#endif
        }

        private static LoginData GetLoginData(bool start)
        {
            return new LoginData
            {
                googleId = PlayerPrefsStorage.GoogleId,
                appleId = PlayerPrefsStorage.AppleId,
                facebookId = PlayerPrefsStorage.FacebookId,
                name = PlayerPrefsStorage.Username,
                deviceId = PlayerPrefsStorage.DeviceId,
                v = PlayerPrefsStorage.Version,
                build = PlayerPrefsStorage.Build,
                language = PlayerPrefsStorage.Language,
                start = start
            };
        }

        public static async Task<LoginResult> SocialAuth(bool silent, bool attach)
        {
#if UNITY_EDITOR
            await Task.Delay(100);
            return LoginResult.Fail;
#else
#if UNITY_ANDROID
            ISocialController social = new GPGSSocialController();
#elif UNITY_IOS
            ISocialController social = new GameCenterSocialController();
#endif
            var loggedResult = LoginResult.Pending;
            social.Init();

            await Task.Delay(100);
            
            social.Login(silent, result =>
            {
                loggedResult = result;
            });

            while (loggedResult == LoginResult.Pending)
            {
                await Task.Delay(100);
            }

            if (loggedResult == LoginResult.Success)
            {
                var userId = UnityEngine.Social.localUser.id;
#if UNITY_ANDROID
                PlayerPrefsStorage.GoogleId = userId;
                if (attach)
                {
                    RestAPI.Attach(AttachType.google, new AttachData
                    {
                        googleId = userId
                    });
                }
#elif UNITY_IOS
                PlayerPrefsStorage.AppleId = userId;
                if (attach)
                {
                    RestAPI.Attach(AttachType.apple, new AttachData
                    {
                        appleId = userId
                    });
                }
#endif
            }
            
            return loggedResult;
#endif
        }
        
        private static async Task<int> GetInstallIdAsync(StatsInstallRequest request)
        {
            var response = await request.GetResponseAsync();
            var id = response.id;
            PlayerPrefsStorage.InstallId = id;
            return id;
        }

        private static async Task<string> GetTokenAsync<TData, TResponse>(AuthRequest<TData, TResponse> request) where TResponse : BaseUserResponse
        {
            var response = await request.GetResponseAsync();
            if (response.code.status == 0)
            {
                var token = response.user.token;
                PlayerPrefsStorage.Token = token;
                PlayerPrefsStorage.UserId = response.user.id;
                PlayerPrefsStorage.Username = response.user.name;
                return token;
            }
            return null;
        }
    }
}