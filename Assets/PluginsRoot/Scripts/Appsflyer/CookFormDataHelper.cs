using System;
using System.Collections.Generic;
using MiniJSON;
using PluginsRoot.Stats;
using PluginsRoot.Tools;
using UnityEngine;

namespace PluginsRoot.Appsflyer
{
	public static class CookFormDataHelper 
	{
		public static AppsflyerData CookFormData(string appsflyer_data, AppsFlyerCallbackStates AF_state)
		{
			var data = new AppsflyerData();
			data.deviceId = AdvertisingIdHelper.DeviceId;
			data.advertising_id = AdvertisingIdHelper.AdvertisingId;

			string market = "";
#if UNITY_ANDROID
			market = "google";
#elif UNITY_IOS
			market = "appstore";
#endif
			data.market = market;
			data.systemLanguage = Application.systemLanguage.ToString();
			data.localTime = StringTools.CookDateTimeString(DateTime.Now);

			if (!string.IsNullOrEmpty(appsflyer_data))
			{
				JSONObject appsflyer_json;
				if (!string.IsNullOrEmpty(appsflyer_data)) 
				{
					appsflyer_json = JSONObject.Parse(appsflyer_data);
				} 
				else 
				{
					appsflyer_json = null;
				}
				data.af_attribution_response = appsflyer_data;
				data.af_status = appsflyer_json != null ? appsflyer_json.GetString("af_status") : string.Empty;

				data.af_sub1 = appsflyer_json != null ? appsflyer_json.GetString("af_sub1") : string.Empty;
				data.af_sub2 = appsflyer_json != null ? appsflyer_json.GetString("af_sub2") : string.Empty;
				string extra;
				if (AF_state == AppsFlyerCallbackStates.DataReceived) 
				{
					extra = appsflyer_json != null ? appsflyer_json.GetString("af_sub3") : string.Empty;
				} 
				else if (AF_state == AppsFlyerCallbackStates.DataError) 
				{
					extra = "1";
				} 
				else 
				{
					extra = "2";
				}
				data.af_sub3 = extra; // Extra
				data.af_sub4 = appsflyer_json != null ? appsflyer_json.GetString("af_sub4") : string.Empty;
				string channel_id;
				if (AF_state == AppsFlyerCallbackStates.DataReceived) 
				{
					channel_id = appsflyer_json != null ? appsflyer_json.GetString("af_sub5") : string.Empty;
				} 
				else 
				{
					channel_id = "-1";
				} 
				data.af_sub5 = channel_id; // Сhannel ID
			}

			string refferal = AdTrackersManager.GetRefferalString();
			if (!string.IsNullOrEmpty(refferal))
			{
				refferal = refferal.Replace(" ","_");
				Dictionary<string, string> parsed_refferal = ParseReferrerString(refferal);
				//data.build = Conf.GetBuild().ToString();
				string email = parsed_refferal.ContainsKey("google_id") ? StringTools.CalculateMD5Hash(parsed_refferal ["google_id"]) : "";
				data.store_email = email;
				data.utm_source = parsed_refferal.ContainsKey("source") ? parsed_refferal["source"] : "";
				data.utm_campaign = parsed_refferal.ContainsKey("campaign") ? parsed_refferal["campaign"] : "";
				data.utm_content = parsed_refferal.ContainsKey("content") ? parsed_refferal["content"] : "";
				data.utm_term = parsed_refferal.ContainsKey("term") ? parsed_refferal["term"] : "";
				data.utm_medium = parsed_refferal.ContainsKey("medium") ? parsed_refferal["medium"] : "";
			}

			data.ram = SystemInfo.systemMemorySize.ToString();
			data.cores = SystemInfo.processorCount.ToString();
			#if UNITY_ANDROID
			try 
			{
				data.cpu = AndroidNativeFunctions.GetMaxCpuFrequency().ToString();
			} 
			catch(Exception e) 
			{
				data.cpu = "0";
			}
			#endif

#if UNITY_ANDROID
            try
            {				
				data.os_version = AndroidNativeFunctions.GetDeviceInfo().RELEASE;
				AndroidOsBuild device_info = AndroidNativeFunctions.GetDeviceInfoBuild();
				if (device_info != null) {
					if (device_info.MANUFACTURER != null)
						data.manufacturer = device_info.MANUFACTURER;
					if (device_info.BRAND != null)
						data.brand = device_info.BRAND;
					if (device_info.PRODUCT != null)
						data.product = device_info.PRODUCT;
					if (device_info.MODEL != null)
						data.model = device_info.MODEL;
					if (device_info.SERIAL != null)
						data.serial = device_info.SERIAL;
					if (device_info.TIME != null)
						data.time = device_info.TIME.ToString();
					if (device_info.BOARD != null)
						data.board = device_info.BOARD;
				}				
			}
            catch (Exception e)
            {
	            // ignored
            }
#endif

#if UNITY_IOS
            data.os_version = SystemInfo.operatingSystem;            

            if (UnityEngine.iOS.Device.vendorIdentifier != null) data.manufacturer = UnityEngine.iOS.Device.vendorIdentifier;
			//if (device_info.BRAND != null) data.brand = device_info.BRAND;
			//if (device_info.PRODUCT != null) data.product = device_info.PRODUCT;
			if (SystemInfo.deviceModel != null) data.model = SystemInfo.deviceModel;
            //if (device_info.SERIAL != null) data.serial = device_info.SERIAL;	
            //data.time = device_info.TIME.ToString();
			//if (device_info.BOARD != null) data.board = device_info.BOARD;
#endif

			return data;
		}


		public static Dictionary<string, string> ParseReferrerString(string url)
		{
			//string data = "?google_id=ognev@skytecgames.ru&ref=Referrer is: utm_source=493&utm_medium=link&utm_content=doff&utm_term=allmobile.ru/&utm_campaign=719";
			string data = url;

			string[] items = data.Split('&');
			Dictionary<string, string> result = new Dictionary<string, string>();

			for(int i=0;i<items.Length;++i) 
			{
				if(items[i].StartsWith("?google_id")) 
				{
					string[] ii = items[i].Split('=');
					if(ii.Length == 2) 
					{
						result.Add("google_id", ii[1]);
					}
				} 
				else if(items[i].Contains("utm_source=")) 
				{
					string[] ii = items[i].Split(new string[] {"utm_source="}, StringSplitOptions.None);
					if(ii.Length == 2) 
					{
						result.Add("source", ii[1]);
					}
				} 
				else if(items[i].Contains("utm_medium=")) 
				{
					string[] ii = items[i].Split(new string[] {"utm_medium="}, StringSplitOptions.None);
					if(ii.Length == 2) 
					{
						result.Add("medium", ii[1]);
					}
				} 
				else if(items[i].Contains("utm_content=")) 
				{
					string[] ii = items[i].Split(new string[] {"utm_content="}, StringSplitOptions.None);
					if(ii.Length == 2) 
					{
						result.Add("content", ii[1]);
					}
				} 
				else if(items[i].Contains("utm_term=")) 
				{
					string[] ii = items[i].Split(new string[] {"utm_term="}, StringSplitOptions.None);
					if(ii.Length == 2) 
					{
						result.Add("term", ii[1]);
					}
				} 
				else if(items[i].Contains("utm_campaign=")) 
				{
					string[] ii = items[i].Split(new string[] {"utm_campaign="}, StringSplitOptions.None);
					if(ii.Length == 2) 
					{
						result.Add("campaign", ii[1]);
					}
				}
			}

			return result;
		}
	}
}