﻿using System;
using UnityEngine;

namespace PluginsRoot.Appsflyer
{
	public static class AdTrackersManager 
	{
		static bool isGoogleIdUpdated = false;
		static string googleId = "";
		static string refferal = "";

		static void UpdateGoogleId()
		{
			Debug.Log ("============================= Update Google Id Start");
#if UNITY_ANDROID && !UNITY_EDITOR
			if (isGoogleIdUpdated) return;
			isGoogleIdUpdated = true;
			try 
			{
				AndroidJavaClass androidJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject jo = androidJC.GetStatic<AndroidJavaObject>("currentActivity");

				AndroidJavaClass ajc = new AndroidJavaClass("com.mycomp.tetatet.OurReceiver");

				refferal = ajc.CallStatic<string>("ReturnString");
				Debug.Log(ajc.CallStatic<string>("ReturnString"));
				Debug.Log ("============================= UpdateGoogleIdFinished");
			}
			catch (Exception e) 
			{
				Debug.Log ("============================= Update googleId Exception: " + e.Message);
			}  
#endif
		}

		public static string GetRefferalString()
		{
			UpdateGoogleId();
#if UNITY_ANDROID && !UNITY_EDITOR
			Debug.Log ("============================= REFFERAL: ?google_id="+googleId+"&ref="+refferal);
			return refferal;
#else
			return "";
#endif
		}
	}
}