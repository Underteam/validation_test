public enum AppsFlyerCallbackStates
{
    NoCallback,
    DataReceived,
    DataError
}