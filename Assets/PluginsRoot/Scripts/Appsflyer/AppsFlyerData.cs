using System;
using PluginsRoot.Stats;

namespace PluginsRoot.Appsflyer
{
    public static class AppsFlyerData
    {
        public static string ReceivedData;
        public static string ErrorText;
        public static AppsFlyerCallbackStates State;
        
        public static AppsflyerData Data { get; private set; }
        
        public static event Action<AppsflyerData> OnReady;

        public static void Complete(AppsflyerData data)
        {
            Data = data;
            OnReady?.Invoke(data);
        }
    }
}