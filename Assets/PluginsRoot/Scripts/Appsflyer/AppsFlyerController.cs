using System;
using AppsFlyerSDK;
using UnityEngine;

namespace PluginsRoot.Appsflyer
{
	public class AppsFlyerController : MonoBehaviour, IAppsFlyerConversionData
	{
		private float afCallback_TimeOut = 180;
		private float afCallback_Timer;
		
		private void Awake()
		{
			DontDestroyOnLoad(this);
			
			enabled = false;
		}

		public void Init(string customerId)
		{
			var config = PluginsConfig.Get().AppsFlyer;

			if (string.IsNullOrEmpty(config.AppKey))
			{
				throw new Exception("AppsFlyer should be configured. Go to PluginsRoot/Edit Settings to configure");
			}
			
			AppsFlyer.setCustomerUserId(customerId);
			AppsFlyer.setIsDebug(config.Debug);
			AppsFlyer.initSDK(config.AppKey, Application.identifier, this);
			AppsFlyer.startSDK();
			
#if !UNITY_EDITOR
			afCallback_Timer = afCallback_TimeOut;
			enabled = true;
#endif
		}
		
		void Update() 
		{
			if (AppsFlyerData.State == AppsFlyerCallbackStates.NoCallback) 
			{
				afCallback_Timer -= Time.deltaTime;
				if (afCallback_Timer > 0) return;
			}
			
			enabled = false;
			
			AppsFlyerData.Complete(CookFormDataHelper.CookFormData(AppsFlyerData.ReceivedData, AppsFlyerData.State));
		}

		public void onConversionDataSuccess(string conversionData)
		{
			AppsFlyerData.ReceivedData = conversionData;
			AppsFlyerData.State = AppsFlyerCallbackStates.DataReceived;
			printCallback("AppsFlyerController:: onConversionDataSuccess = " + conversionData);
		}

		public void onConversionDataFail(string error)
		{
			AppsFlyerData.ErrorText = error;
			AppsFlyerData.State = AppsFlyerCallbackStates.DataError;
			printCallback("AppsFlyerController:: onConversionDataFail = " + error);
		}

		public void onAppOpenAttribution(string attributionData)
		{
			printCallback("AppsFlyerController:: onAppOpenAttribution  = " + attributionData);
		}

		public void onAppOpenAttributionFailure(string error)
		{
			printCallback("AppsFlyerController:: onAppOpenAttributionFailure = " + error);
		}
		
		void printCallback(string str)
		{
			Debug.Log(str);
		}
	}
}