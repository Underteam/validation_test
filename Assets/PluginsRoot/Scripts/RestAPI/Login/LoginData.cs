using System;

namespace PluginsRoot.RestAPI
{
    [Serializable]
    public class LoginData
    {
        public string appleId;
        public int build;
        public string deviceId;
        public string facebookId;
        public string googleId;
        public string language;
        public string name;
        public bool start;
        public int v;
    }
}