namespace PluginsRoot.RestAPI
{
    public class AuthLoginRequest : AuthRequest<LoginData, LoginResponse>
    {
        public AuthLoginRequest(LoginData data) : base(data, "/login", "login")
        {
        }
    }
}