namespace PluginsRoot.RestAPI
{
    public class AuthTestRequest : AuthRequest<object, BaseUserResponse>
    {
        public AuthTestRequest() : base("/test", "test")
        {
        }
    }
}