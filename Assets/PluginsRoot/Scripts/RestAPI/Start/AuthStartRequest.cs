namespace PluginsRoot.RestAPI
{
    public class AuthStartRequest : AuthRequest<StartData, StartResponse>
    {
        public AuthStartRequest(StartData data) : base(data, "/start", "start")
        {
        }
    }
}