using System;

namespace PluginsRoot.RestAPI
{
    [Serializable]
    public class StartData
    {
        public int build;
        public string deviceId;
        public string language;
        public bool start;
        public int v;
    }
}