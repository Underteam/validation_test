namespace PluginsRoot.RestAPI
{
    public class PayRequest : PayRequest<PayData, PayResult>
    {
        public PayRequest(PayMarket market, PayData data) : base(data, "/" + market.ToString("G"), null)
        {
        }
    }
}