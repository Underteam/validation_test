using System;
using MiniJSON;

namespace PluginsRoot.RestAPI
{
    [Serializable]
    public class PayResult : BaseResponse
    {
        public string error;
        public bool firstBuy;
        public string originalPaymentId;
        public long paymentId;
        public bool processed;
        public long purchaseDate;
        public bool success;
    }
}