using System;

namespace PluginsRoot.RestAPI
{
    [Serializable]
    public class PayData
    {
        public string bill;
        public string currency;
        public string payload;
        public double price;
        public string productId;
        public string sig;
        public bool test;
    }
}