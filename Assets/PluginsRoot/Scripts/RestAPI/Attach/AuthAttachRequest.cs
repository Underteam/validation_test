namespace PluginsRoot.RestAPI
{
    public class AuthAttachRequest : AuthRequest<AttachData, BaseUserResponse>
    {
        public AuthAttachRequest(AttachType type, AttachData data) : base(data, "/attach/" + type.ToString("G"), "attach")
        {
        }
    }
}