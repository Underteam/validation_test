namespace PluginsRoot.RestAPI
{
    public class AuthDetachRequest : AuthRequest<DetachData, BaseUserResponse>
    {
        public AuthDetachRequest(AttachType type) : base("/detach/" + type.ToString("G"), "detach")
        {
        }
    }
}