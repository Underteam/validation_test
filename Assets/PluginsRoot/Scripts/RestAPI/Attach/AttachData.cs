using System;

namespace PluginsRoot.RestAPI
{
    [Serializable]
    public class AttachData
    {
        public string appleId;
        public string facebookAccessToken;
        public string facebookId;
        public string googleId;
    }
}