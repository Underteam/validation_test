using System.Collections.Generic;
using PluginsRoot.Stats;

namespace PluginsRoot.RestAPI
{
    public interface IRestAPI
    {
        AuthTestRequest Test();
        AuthLoginRequest Login(LoginData data);
        AuthStartRequest Start(StartData data);
        AuthAttachRequest Attach(AttachType type, AttachData data);
        AuthDetachRequest Detach(AttachType type);
        
        StatsInstallRequest Install(StatsInstallData data);
        StatsRegRequest Reg(StatsRegData data);
        
        PayRequest Payment(PayData data);
    }
}