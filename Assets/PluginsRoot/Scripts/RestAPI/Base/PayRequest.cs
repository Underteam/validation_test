﻿namespace PluginsRoot.RestAPI
{
    public class PayRequest<TData, TResponse> : BaseRequest<TData, TResponse> where TResponse : BaseResponse
    {
        public override string RequestGroup => "/pay";
        
        public PayRequest(string path, string tag) : base(path, tag)
        {
        }

        public PayRequest(TData data, string path, string tag) : base(data, path, tag)
        {
        }
    }
}