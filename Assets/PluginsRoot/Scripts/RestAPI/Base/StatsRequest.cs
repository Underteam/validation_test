namespace PluginsRoot.RestAPI
{
    public abstract class StatsRequest<TData, TResponse> : BaseRequest<TData, TResponse> where TResponse : BaseResponse
    {
        public override string RequestGroup => "/stats";

        public StatsRequest(string path, string tag) : base(path, tag)
        {
        }

        public StatsRequest(TData data, string path, string tag) : base(data, path, tag)
        {
        }
    }
}