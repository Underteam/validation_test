namespace PluginsRoot.RestAPI
{
    public abstract class AuthRequest<TData, TResponse> : BaseRequest<TData, TResponse> where TResponse : BaseResponse
    {
        public override string RequestGroup => "/auth";
        
        public AuthRequest(string path, string tag) : base(path, tag)
        {
        }

        public AuthRequest(TData data, string path, string tag) : base(data, path, tag)
        {
        }
    }
}