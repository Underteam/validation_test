using System;

namespace PluginsRoot.RestAPI
{
    [Serializable]
    public class BaseUserResponse : BaseResponse
    {
        public UserResponse user;
    }

    [Serializable]
    public class UserResponse
    {
        public int id;
        public string name;
        public string deviceId;
        public int[] createdAt;
        public string language;
        public string version;
        public string build;
        public string token;
        public int[] lastVisit;
        public string sumDonate;
        
        public string appleId;
        public string facebookId;
        public string googleId;
    }
}