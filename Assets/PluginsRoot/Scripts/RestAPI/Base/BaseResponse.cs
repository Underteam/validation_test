using System;

namespace PluginsRoot.RestAPI
{
    [Serializable]
    public class BaseResponse
    {
        public ErrorCode code;
    }

    [Serializable]
    public class ErrorCode
    {
        public string message;
        public int status;
    }
}