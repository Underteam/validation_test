using System.Threading.Tasks;
using PluginsRoot.Network;
using UnityEngine;

namespace PluginsRoot.RestAPI
{
    public abstract class BaseRequest<TData, TResponse> where TResponse : BaseResponse
    {
        public readonly TData Data;
        public TResponse Response { get; protected set; }

        public readonly Request Request;

        public virtual string RequestGroup => "";
        
        public BaseRequest(string path, string tag)
        {
            Request = new Request(RequestGroup + path, tag, ResponseHandler);
        }

        public BaseRequest(TData data, string path, string tag)
        {
            Data = data;
            
            Request = new Request(RequestGroup + path, tag, ResponseHandler);
            Request.SetPostData(JsonUtility.ToJson(Data));
        }

        public async Task<TResponse> GetResponseAsync()
        {
            while (Response == null)
            {
                await Task.Delay(100);
            }
            return Response;
        }
        
        private void ResponseHandler(string response)
        {
            Response = JsonUtility.FromJson<TResponse>(response);
        }
    }
}