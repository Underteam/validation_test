using PluginsRoot.Stats;
using PluginsRoot.Network;

namespace PluginsRoot.RestAPI
{
    public class SkytecRestAPI : IRestAPI
    {
        private NetSystem _netSystem;
        
        private string _token;
        private PayMarket _market;

        public SkytecRestAPI(NetSystem netSystem)
        {
            _netSystem = netSystem;
        }
        
        public void SetToken(string token)
        {
            _token = token;
            _netSystem.SetAuthToken(token);
        }

        public void SetMarket(PayMarket market)
        {
            _market = market;
        }
        
        public AuthTestRequest Test()
        {
            var request = new AuthTestRequest();
            _netSystem.AddRequest(request.Request);
            return request;
        }
        
        public AuthLoginRequest Login(LoginData data)
        {
            var request = new AuthLoginRequest(data);
            _netSystem.AddRequest(request.Request);
            return request;
        }

        public AuthStartRequest Start(StartData data)
        {
            var request = new AuthStartRequest(data);
            _netSystem.AddRequest(request.Request);
            return request;
        }

        public AuthAttachRequest Attach(AttachType type, AttachData data)
        {
            var request = new AuthAttachRequest(type, data);
            _netSystem.AddRequest(request.Request);
            return request;
        }
        
        public AuthDetachRequest Detach(AttachType type)
        {
            var request = new AuthDetachRequest(type);
            _netSystem.AddRequest(request.Request);
            return request;
        }
        
        public StatsInstallRequest Install(StatsInstallData data)
        {
            var request = new StatsInstallRequest(data);
            _netSystem.AddRequest(request.Request);
            return request;
        }
        
        public StatsRegRequest Reg(StatsRegData data)
        {
            var request = new StatsRegRequest(data);
            _netSystem.AddRequest(request.Request);
            return request;
        }

        public PayRequest Payment(PayData data)
        {
            var request = new PayRequest(_market, data);
            _netSystem.AddRequest(request.Request);
            return request;
        }
    }
}