using PluginsRoot.RestAPI;

namespace PluginsRoot.Stats
{
    public class StatsInstallRequest : StatsRequest<StatsInstallData, StatsInstallResponse>
    {
        public StatsInstallRequest(StatsInstallData data) : base(data, "/install", "install")
        {
        }
    }
}