﻿namespace PluginsRoot.Stats
{
    public class AppsflyerData
    {
        public string deviceId;
        public string language;
        public int start;
        public string version;
        
        public string advertising_id;
        public string board;
        public string brand;
        public string build;
        public string cores;
        public string country;
        public string cpu;
        public string localTime;
        public string manufacturer;
        public string market;
        public string model;
        public string operatorCode;
        public string operatorName;
        public string os_version;
        public string phoneNumber;
        public string product;
        public string ram;
        public string serial;
        public string store_email;
        public string systemLanguage;
        public string time;
        public string utm_campaign;
        public string utm_content;
        public string utm_medium;
        public string utm_source;
        public string utm_term;
        
        public string af_attribution_response;
        public string af_status;
        public string af_sub1;
        public string af_sub2;
        public string af_sub3;
        public string af_sub4;
        public string af_sub5;
    }
}