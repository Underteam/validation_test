using PluginsRoot.RestAPI;

namespace PluginsRoot.Stats
{
    public class StatsRegRequest : StatsRequest<StatsRegData, BaseResponse>
    {
        public StatsRegRequest(StatsRegData data) : base(data, "/reg", "reg")
        {
        }
    }
}