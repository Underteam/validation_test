using System;

namespace PluginsRoot.Stats
{
    [Serializable]
    public class StatsRegData
    {
        public string advertising_id;
        public string af_attribution_response;
        public string af_status;
        public string af_sub1;
        public string af_sub2;
        public string af_sub3;
        public string af_sub4;
        public string af_sub5;
        public string board;
        public string brand;
        public string build;
        public string cores;
        public string country;
        public string cpu;
        public string localTime;
        public string manufacturer;
        public string market;
        public string model;
        public string operatorCode;
        public string operatorName;
        public string os_version;
        public string phoneNumber;
        public string product;
        public string ram;
        public string serial;
        public string store_email;
        public string systemLanguage;
        public string time;
        public string utm_campaign;
        public string utm_content;
        public string utm_medium;
        public string utm_source;
        public string utm_term;

        public StatsRegData(AppsflyerData data)
        {
            advertising_id = data.advertising_id;
            af_attribution_response = data.af_attribution_response;
            af_status = data.af_status;
            af_sub1 = data.af_sub1;
            af_sub2 = data.af_sub2;
            af_sub3 = data.af_sub3;
            af_sub4 = data.af_sub4;
            af_sub5 = data.af_sub5;
            board = data.board;
            brand = data.brand;
            build = data.build;
            cores = data.cores;
            country = data.country;
            cpu = data.cpu;
            localTime = data.localTime;
            manufacturer = data.manufacturer;
            market = data.market;
            model = data.model;
            operatorCode = data.operatorCode;
            operatorName = data.operatorName;
            os_version = data.os_version;
            phoneNumber = data.phoneNumber;
            product = data.product;
            ram = data.ram;
            serial = data.serial;
            store_email = data.store_email;
            systemLanguage = data.systemLanguage;
            time = data.time;
            utm_campaign = data.utm_campaign;
            utm_content = data.utm_content;
            utm_medium = data.utm_medium;
            utm_source = data.utm_source;
            utm_term = data.utm_term;
        }
    }
}