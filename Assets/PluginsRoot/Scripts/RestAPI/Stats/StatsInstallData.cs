using System;

namespace PluginsRoot.Stats
{
    [Serializable]
    public class StatsInstallData
    {
        public string advertising_id;
        public string board;
        public string brand;
        public string build;
        public string cores;
        public string country;
        public string cpu;
        public string deviceId;
        public string language;
        public string localTime;
        public string manufacturer;
        public string market;
        public string model;
        public string operatorCode;
        public string operatorName;
        public string os_version;
        public string phoneNumber;
        public string product;
        public string ram;
        public string serial;
        public int start;
        public string store_email;
        public string systemLanguage;
        public string time;
        public string utm_campaign;
        public string utm_content;
        public string utm_medium;
        public string utm_source;
        public string utm_term;
        public string version;

        public StatsInstallData(AppsflyerData data, bool start)
        {
            advertising_id = data.advertising_id;
            board = data.board;
            brand = data.brand;
            build = data.build;
            cores = data.cores;
            country = data.country;
            cpu = data.cpu;
            deviceId = data.deviceId;
            language = data.language;
            localTime = data.localTime;
            manufacturer = data.manufacturer;
            market = data.market;
            model = data.model;
            operatorCode = data.operatorCode;
            operatorName = data.operatorName;
            os_version = data.os_version;
            phoneNumber = data.phoneNumber;
            product = data.product;
            ram = data.ram;
            serial = data.serial;
            this.start = start ? 1 : 0;
            store_email = data.store_email;
            systemLanguage = data.systemLanguage;
            time = data.time;
            utm_campaign = data.utm_campaign;
            utm_content = data.utm_content;
            utm_medium = data.utm_medium;
            utm_source = data.utm_source;
            utm_term = data.utm_term;
            version = data.version;
        }
    }
}