﻿using System;
using PluginsRoot.RestAPI;

namespace PluginsRoot.Stats
{
    [Serializable]
    public class StatsInstallResponse : BaseResponse
    {
        public int id;
    }
}