﻿using UnityEngine;
using UnityEngine.Purchasing;

namespace PluginsRoot.Payment
{
    public class PaymentBehaviour : MonoBehaviour
    {
        public void OnPurchaseComplete(Product product)
        {
            PaymentProcess.OnPurchaseComplete(product);
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
        {
            PaymentProcess.OnPurchaseFailed(product, reason);
        }
    }
}