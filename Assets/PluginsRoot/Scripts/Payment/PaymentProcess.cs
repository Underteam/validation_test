﻿using System;
using System.Collections.Generic;
using PluginsRoot.RestAPI;
using UnityEngine;
using UnityEngine.Purchasing;
#if !UNITY_EDITOR
using MiniJSON;
#endif

namespace PluginsRoot.Payment
{
    public static class PaymentProcess
    {
        private static IRestAPI _restApi;
        
        private static Queue<Product> _pendingProducts = new Queue<Product>();

        public static event Action<string, bool> OnProcessItem;

        public static void Initialize(IRestAPI restApi)
        {
            _restApi = restApi;
            
            while (_pendingProducts.Count > 0)
            {
                OnPurchaseComplete(_pendingProducts.Dequeue());
            }
        }
        
        public static void OnPurchaseComplete(Product product)
        {
            Debug.Log($"OnPurchaseComplete {ProductToString(product)}");
            if (_restApi == null)
            {
                _pendingProducts.Enqueue(product);
                Debug.Log("PaymentProcess wasn't initialized yet. Product is added to the pending queue");
                return;
            }
            
            var payData = ParseProduct(product);
            var request = _restApi.Payment(payData).GetResponseAsync();
            
            request.GetAwaiter().OnCompleted(() =>
            {
                if(request.Result == null)
                    Debug.LogError("Server result null");
                else
                    Debug.LogError("Server result " + request.Result.code + " " + request.Result.error + " " + request.Result.success + " " + request.Result.processed);

                OnProcessItem?.Invoke(payData.productId, true);
                CodelessIAPStoreListener.Instance.StoreController.ConfirmPendingPurchase(product);
            });
        }

        private static string ProductToString(Product product)
        {
            return "{ productId: " + product.definition.id + ", " +
                "transactionId: " + product.transactionID + ", " +
                "hasReceipt: " + product.hasReceipt + ", " +
                "receipt: " + product.receipt + "" +
            "}";
        }

        private static PayData ParseProduct(Product product)
        {
#if UNITY_EDITOR
            var billJson = "";
            var devPayload = "";
            var sig = "";
            var test = true;
#elif UNITY_ANDROID
            var receipt = JSONObject.Parse(product.receipt);
            var payloadJson = receipt.GetString("Payload");
            var payload = JSONObject.Parse(payloadJson);
            var billJson = payload.GetString("json");
            var sig = payload.GetString("signature");
            var bill = JSONObject.Parse(billJson);
            var devPayload = bill.GetString("developerPayload");
            var test = false;
#elif UNITY_IOS
            // TODO: Not sure if it is ok
            var receipt = JSONObject.Parse(product.receipt);
            var payload = receipt.GetString("Payload");
            //var payloadJson = JSONObject.Parse(payload);
            var billJson = payload;
            // https://docs.unity3d.com/Manual/UnityIAPPurchaseReceipts.html
            //if (payloadJson != null && payload.ContainsKey("bundle_id")) // App Receipt, iOS >= 7
            //{
            //    billJson = payloadJson;
            //    // in Cards: devPayload = PlayerPrefs.GetString($"payload_{productId}");
            //}
            //else // SKPaymentTransaction transactionReceipt, iOS < 7
            //{
            //    billJson = payload.GetObject("payment").ToString();
            //}
            var devPayload = ""; // google only?
            var sig = ""; // google only feature
            var test = false;
#endif

            return new PayData
            {
                bill = billJson,
                currency = product.metadata.isoCurrencyCode,
                payload = devPayload,
                price = (double) product.metadata.localizedPrice,
                sig = sig,
                productId = product.definition.storeSpecificId,
                test = test
            };
        }

        public static void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
        {
            var payData = ParseProduct(product);
            OnProcessItem?.Invoke(payData.productId, false);
            if (_restApi == null)
            {
                Debug.Log("PaymentProcess wasn't initialized yet.");
            }
            
            Debug.Log("Product purchase failed, reason: " + reason);
        }
    }
}