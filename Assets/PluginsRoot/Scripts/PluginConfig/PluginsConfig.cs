using System;
using UnityEngine;
using PluginsRoot.Utils;
using Environment = PluginsRoot.Network.Environment;

namespace PluginsRoot
{
    [Serializable]
    public class PluginsConfig : ScriptableObject
    {
        public const string PluginsConfigAssetName = "PluginsConfig";
        public const string PluginsConfigAssetExtension = ".asset";
        public static string PluginsConfigFullPath => PluginsConfigAssetName + PluginsConfigAssetExtension;
        
        public AppsFlyerConfig AppsFlyer = new AppsFlyerConfig();
        public AppLovinConfig AppLovin = new AppLovinConfig();
        public ServerConfig Server = new ServerConfig();

        private static PluginsConfig _instance;
        public static PluginsConfig Get()
        {
            if (_instance != null) return _instance;

            _instance = Resources.Load<PluginsConfig>(PluginsConfigAssetName);
            if (_instance == null)
            {
                _instance = ScriptableObjectUtility.CreateAsset<PluginsConfig>(PluginsConfigFullPath);
            }
            return _instance;
        }

        public static void Save()
        {
            ScriptableObjectUtility.SaveAssets();
        }
    }

    [Serializable]
    public class AppsFlyerConfig
    {
        public string AppId;
        public string AppKey = "upqXCHz86iM7Pe6xT7e8YL";
        public bool Debug;
    }
    
    [Serializable]
    public class AppLovinConfig
    {
        public string SdkKey = "5NQJ-Go6iJhpS_yCFNB-aRIG14EBroCx9cE7u0EbKwOnvL-8i97IbPTcUooKZ-FkbTsUU44q_TaupT1Wi9pIZM";
        public string AdUnit_Android = "";
        public string AdUnit_iOS = "";
    }
    
    [Serializable]
    public class ServerConfig
    {
        public int EnvironmentId;
        public string BaseUrlProduction;
        public string BaseUrlDevelop;
        public string BaseUrlLocal;
        public string ApiUrl = "/api/v";
        public int ApiVersion = 1;

        public string GetFullUrl(int environmentId)
        {
            return GetFullUrl((Environment) environmentId);
        }
        
        public string GetFullUrl(Environment environment = Environment.Develop)
        {
            switch (environment)
            {
                case Environment.Local:
                    return BaseUrlLocal + ApiUrl + (ApiVersion > 0 ? "" + ApiVersion : "");
                case Environment.Develop:
                    return BaseUrlDevelop + ApiUrl + (ApiVersion > 0 ? "" + ApiVersion : "");
                case Environment.Production:
                    return BaseUrlProduction + ApiUrl + (ApiVersion > 0 ? "" + ApiVersion : "");
            }
            return null;
        }

        public readonly string[] Environments = Enum.GetNames(typeof(Environment));
    }
}