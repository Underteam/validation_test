using UnityEditor;
using UnityEngine;

namespace PluginsRoot
{
    [CustomEditor(typeof(PluginsConfig))]
    public class PluginsWindow : Editor
    {
        private bool _AppsFlyerFoldout = true;
        private bool _AppLovinFoldout = true;
        private bool _ServerFoldout = true;
        
        [MenuItem("PluginsRoot/Edit Settings")]
        public static void ShowWindow()
        {
            Selection.activeObject = PluginsConfig.Get();
        }
        
        public override void OnInspectorGUI()
        {
            var config = PluginsConfig.Get();
            
            _AppsFlyerFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(_AppsFlyerFoldout, "AppsFlyer");
            if (_AppsFlyerFoldout)
            {
                EditorUtility.SetDirty(config);
                
                config.AppsFlyer.AppId = EditorGUILayout.TextField(" - App Id (iOS only)", config.AppsFlyer.AppId);
                config.AppsFlyer.AppKey = EditorGUILayout.TextField(" - App Key", config.AppsFlyer.AppKey);
                config.AppsFlyer.Debug = EditorGUILayout.Toggle(" - Debug", config.AppsFlyer.Debug);
            }
            EditorGUILayout.EndFoldoutHeaderGroup();
            
            _AppLovinFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(_AppLovinFoldout, "AppLovin");
            if (_AppLovinFoldout)
            {
                EditorUtility.SetDirty(config);
                
                config.AppLovin.SdkKey = EditorGUILayout.TextField(" - Sdk Key", config.AppLovin.SdkKey);
                config.AppLovin.AdUnit_Android = EditorGUILayout.TextField(" - Ad Unit Android", config.AppLovin.AdUnit_Android);
                config.AppLovin.AdUnit_iOS = EditorGUILayout.TextField(" - Ad Unit iOS", config.AppLovin.AdUnit_iOS);
            }
            EditorGUILayout.EndFoldoutHeaderGroup();
            
            _ServerFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(_ServerFoldout, "Game Server");
            if (_ServerFoldout)
            {
                EditorUtility.SetDirty(config);
                
                config.Server.EnvironmentId = EditorGUILayout.Popup(" - Environment", config.Server.EnvironmentId, config.Server.Environments);
                config.Server.BaseUrlProduction = EditorGUILayout.TextField(" - Base URL (Production)", config.Server.BaseUrlProduction);
                config.Server.BaseUrlDevelop = EditorGUILayout.TextField(" - Base URL (Develop)", config.Server.BaseUrlDevelop);
                config.Server.BaseUrlLocal = EditorGUILayout.TextField(" - Base URL (Local)", config.Server.BaseUrlLocal);
                config.Server.ApiUrl = EditorGUILayout.TextField(" - API URL", config.Server.ApiUrl);
                config.Server.ApiVersion = EditorGUILayout.IntField(" - API version", config.Server.ApiVersion);
                EditorGUILayout.LabelField(" - Example", config.Server.GetFullUrl(config.Server.EnvironmentId));
            }
            EditorGUILayout.EndFoldoutHeaderGroup();
            
            if (GUILayout.Button("Save"))
            {
                PluginsConfig.Save();
            }
        }
    }
}