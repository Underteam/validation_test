using GameCore.Common;
using PluginsRoot.Appsflyer;
using PluginsRoot.Network;
using PluginsRoot.Payment;
using PluginsRoot.Tools;
using UnityEngine;

namespace PluginsRoot
{
    public class StartUp : MonoBehaviour
    {
        private static StartUp instance;

        public AppsFlyerController AppsFlyer;
        public PaymentBehaviour IAP;
        
        private NetSystem _netSystem;
        
        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
                return;
            }
                
            DontDestroyOnLoad(this);
            _netSystem = gameObject.AddComponent<NetSystem>();
            gameObject.AddComponent<AdvertisingIdHelper>();
            instance = this;
        }

        private void Start()
        {
            Initializer.OnInstall += OnInstall;
            Initializer.OnComplete += OnComplete;
            Initializer.InitializeAsync(_netSystem);
            AppLovinController.Initialize();
        }

        private void OnInstall(int installId)
        {
            var af = Instantiate(AppsFlyer);
            af.name = AppsFlyer.name;
            af.Init(installId.ToString());
            DontDestroyOnLoad(af.gameObject);
        }

        private void OnComplete()
        {
            var iap = Instantiate(IAP);
            iap.name = IAP.name;
            DontDestroyOnLoad(iap.gameObject);
        }

        private void OnDestroy()
        {
            if (instance == this)
            {
                instance = null;
            }
        }
    }
}