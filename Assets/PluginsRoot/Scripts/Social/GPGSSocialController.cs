﻿#if UNITY_ANDROID
using System;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;

namespace PluginsRoot.Social
{
    public class GPGSSocialController : ISocialController
    {
        public void Init()
        {
            PlayGamesPlatform.Activate();
        }

        public void Login(bool silent, Action<LoginResult> onResult)
        {
            PlayGamesPlatform.Instance.Authenticate(silent ? SignInInteractivity.CanPromptOnce : SignInInteractivity.CanPromptAlways, result =>
            {
                Debug.Log(result);
                onResult?.Invoke(result == SignInStatus.Success ? LoginResult.Success : LoginResult.Fail);
            });
        }
        
        public void Logout()
        {
            PlayGamesPlatform.Instance.SignOut();
        }
    }
}
#endif