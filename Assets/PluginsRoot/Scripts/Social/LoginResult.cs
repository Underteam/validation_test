﻿namespace PluginsRoot.Social
{
    public enum LoginResult
    {
        Pending,
        Success,
        Fail
    }
}