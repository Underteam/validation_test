﻿using System;

namespace PluginsRoot.Social
{
    public interface ISocialController
    {
        void Init();
        void Login(bool silent, Action<LoginResult> onResult);
        void Logout();
    }
}