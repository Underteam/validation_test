#if UNITY_IOS
using System;
using UnityEngine;
namespace PluginsRoot.Social
{
    public class GameCenterSocialController : ISocialController
    {
        public void Init()
        {
            // PlayGamesPlatform.Activate();
        }
        public void Login(bool silent, Action<LoginResult> onResult)
        {
            UnityEngine.Social.localUser.Authenticate(result =>
            {
                Debug.Log(result);
                onResult?.Invoke(result ? LoginResult.Success : LoginResult.Fail);
            });
        }
        public void Logout()
        {
            // PlayGamesPlatform.Instance.SignOut();
        }
    }
}
#endif