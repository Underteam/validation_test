using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MiniJSON;
using UnityEngine;
using UnityEngine.Networking;

namespace PluginsRoot.Network
{
    public class Request
    {
        public string url; // Относительный адрес запроса
        public string tag; // Тэг запроса для избежания двойных запросов там где это не нужно

        string postData;
        Dictionary<string, string> getParams; // Список параметров передающихся посредством GET запроса  ==  ключ/значение
        Dictionary<string, string> postParams; // Список параметров передающихся посредством POST запроса  ==  ключ/значение
        Dictionary<string, string> headers = new Dictionary<string, string>
        {
        }; // Список параметров передающихся посредством заголовков запроса  ==  ключ/значение

        public Action<string> action;

        public UnityWebRequest GetRequest(string requestUrl, bool forcePost)
        {
            if (forcePost || postData != null)
            {
                var uwr = new UnityWebRequest(requestUrl, "POST");
                if (postData != null)
                {
                    byte[] bodyRaw = Encoding.UTF8.GetBytes(postData);
                    uwr.uploadHandler = new UploadHandlerRaw(bodyRaw);
                }
                uwr.downloadHandler = new DownloadHandlerBuffer();
                uwr.SetRequestHeader("Content-Type", "application/json");
                Debug.Log(url + " request: " + postData);
                return uwr;
            }
            
            if (postParams != null) return UnityWebRequest.Post(requestUrl, postParams);
            return UnityWebRequest.Get(requestUrl);
        }
        
        public string response;

        public Request(string url)
        {
            this.url = url;
            this.tag = null;
            this.action = null;
        }

        public Request(string url, string tag, Action<string> action)
        {
            this.url = url;
            this.tag = tag;
            this.action = action;
        }

        public void AddToken(string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                AddHeader("token", token);
            }
        }
        
        public void AddHeader(string key, string value) // === Добавляем заголовок
        {
            if (!headers.ContainsKey(key))
            {
                headers.Add(key, value);
            }
        }

        public void SetPostData(string json)
        {
            if (postParams != null) throw new Exception("Request already has Post params");
            postData = json;
        }

        public void AddParamsGet(string key, string value) // === Добавляем GET параметр
        {
            if (getParams == null)
            {
                getParams = new Dictionary<string, string>();
            }

            if (!getParams.ContainsKey(key))
            {
                getParams.Add(key, value);
            }
        }

        public void AddParamsPost(Dictionary<string, string> dict) // === Добавляем GET параметры
        {
            if (postData != null) throw new Exception("Request already has Post raw data");
            if (postParams == null)
            {
                postParams = new Dictionary<string, string>();
            }

            foreach (string item in dict.Keys.ToList())
            {
                postParams.Add(item, dict[item]);
            }
        }

        public void AddParamsPost(string key, string value) // === Добавляем POST параметр
        {
            if (postData != null) throw new Exception("Request already has Post raw data");
            if (postParams == null)
            {
                postParams = new Dictionary<string, string>();
            }

            if (!postParams.ContainsKey(key))
            {
                postParams.Add(key, value);
            }
        }

        public string GetUrlWithGetParams() // === Получаем URL ссылку вместе с GET параметрами
        {
            if (getParams == null || getParams.Count == 0) return url;

            string fullUrl = url;
            foreach (string item in getParams.Keys.ToList())
            {
                if (fullUrl.IndexOf("?") <= 0) fullUrl += "?";
                else fullUrl += "&";
                fullUrl += item + "=" + getParams[item];
            }

            return fullUrl;
        }

        public Dictionary<string, string> GetHeaders()
        {
            return headers;
        }
    }
}