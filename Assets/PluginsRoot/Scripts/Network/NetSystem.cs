using System.Collections;
using System.Collections.Generic;
using MiniJSON;
using UnityEngine;

namespace PluginsRoot.Network
{
    public class NetSystem : MonoBehaviour
    {
        private string _baseUrl;
        private string _authToken;

        // ================== СПИСОК ЗАПРОСОВ И МЕТОДОВ ДОБАВЛЕНИЯ ЗАПРОСОВ В ЭТОТ СПИСОК =======================
        Queue<Request> requestsQueue = new Queue<Request>();

        public void SetBaseUrl(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        void Start()
        {
            // Запускаем корутину, которая работает на протяжении всей игры и шлёт запросы на сервер
            StartCoroutine(Process()); 
        }

        public void AddRequest(Request newRequest) // === Добавяем новый запрос в очередь запросов
        {
            foreach (Request item in requestsQueue)
            {
                // === Проверка на дубли тэгов для исключения двойных запросов
                if (!string.IsNullOrEmpty(item.tag) && (string.Compare(item.tag, newRequest.tag) == 0))
                {
                    Debug.LogError("Дублирование запроса");
                    return;
                }
            }
            requestsQueue.Enqueue(newRequest); // Если всё хорошо, то добавляем запрос в очередь
        }

        public void SetAuthToken(string token)
        {
            _authToken = token;
        }

        private IEnumerator Process()
        {
            int trySendCount = 0; // Счётчик повторных отправок запроса
            while (true) // Зацикливаем корутину
            {
                while (requestsQueue.Count <= 0)
                {
                    yield return new WaitForSeconds(0.5f);
                }

                bool isSuccess = false; // Индикатор успешности запроса
                // === Получаем первый запрос из очереди, шлём на сервер и дожидаемся ответа
                Request currentRequest = requestsQueue.Peek();
                
                string url = _baseUrl + currentRequest.GetUrlWithGetParams();

                // Отправка запроса на сервер
                using (var downloader = currentRequest.GetRequest(url, true))
                {
                    if (!string.IsNullOrEmpty(_authToken))
                    {
                        downloader.SetRequestHeader("token", _authToken);
                    }
                    
                    downloader.SendWebRequest();
                    
                    float timeRequestStart = Time.realtimeSinceStartup;

                    float maxTime = url.IndexOf("pay") > 0 ? 15f : 3f;
                    // Ждём пока сервер не ответит или пока время ожидания не закончится
                    while (!downloader.isDone) 
                    {
                        if (Time.realtimeSinceStartup - timeRequestStart >= maxTime)
                        {
                            Debug.LogError("Request TimeOut [" + url + "]");
                            break;
                        }

                        yield return new WaitForSeconds(0.1f);
                    }

                    var response = downloader.downloadHandler.text;
                    Debug.Log(currentRequest.url + " response: " + response);
                    
                    if (string.IsNullOrEmpty(downloader.error))
                    {
                        isSuccess = downloader.isDone;

                        if (isSuccess)
                        {
                            var parsed = JSONObject.Parse(downloader.downloadHandler.text);
                            if (parsed != null)
                            {
                                currentRequest.response = response;
                                if (!CheckError(parsed))
                                {
                                    currentRequest.action?.Invoke(response);
                                }
                            }
                        }
                    }

                    // Если запрос прошёл успешно или количество переотправок слишком много,
                    // то удаляем первый запрос из очереди и переходим к следующему
                    if (isSuccess || trySendCount >= 2)
                    {
                        trySendCount = 0;
                        if (requestsQueue.Count > 0 && requestsQueue.Contains(currentRequest))
                        {
                            requestsQueue.Dequeue();
                        }
                    }
                    else // === Иначе не удаляем запрос, но инкрементируем счётчик переотправлений данного запроса
                    {
                        trySendCount++;
                    }

                    yield return new WaitForSeconds(0.05f);
                }
            }
        }

        // ==== Проверка JSON на наличие предопределенной ошибки
        public static bool CheckError(JSONObject response) 
        {
            if (response.ContainsKey("error") && !string.IsNullOrEmpty(response.GetString("error")))
            {
                Debug.LogError("Server send error: " + response.GetString("error"));
                return true;
            }

            return false;
        }
    }
}