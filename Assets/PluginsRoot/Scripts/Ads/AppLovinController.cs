using System;
using PluginsRoot;
using UnityEngine;

namespace GameCore.Common
{
    public static class AppLovinController
    {
        private static string rewardedAdUnitId;

        private static bool _waitingForLoading;
        private static Action<AdsResult> _onShow;

        public static void Initialize()
        {
            var config = PluginsConfig.Get().AppLovin;
            if (config.SdkKey == null)
            {
                Debug.LogError("AppLovin SDK Key should be set");
                return;
            }
            
#if UNITY_ANDROID
            rewardedAdUnitId = config.AdUnit_Android;
#elif UNITY_IOS
            rewardedAdUnitId = config.AdUnit_iOS;
#endif

            if (string.IsNullOrEmpty(rewardedAdUnitId))
            {
                Debug.LogError("AppLovin Ad Unit should be set");
            }
            else
            {
                MaxSdkCallbacks.OnSdkInitializedEvent += sdkConfiguration =>
                {
                    // AppLovin SDK is initialized, start loading ads
                    InitializeRewardedAds();
                };
            }
            
            MaxSdk.SetSdkKey(config.SdkKey);
            MaxSdk.InitializeSdk();
        }

        public static void SetUserId(string userId)
        {
            MaxSdk.SetUserId(userId);
        }

        public static void Show(Action<AdsResult> onShow)
        {
            _onShow = onShow;
            if (MaxSdk.IsRewardedAdReady(rewardedAdUnitId))
            {
                MaxSdk.ShowRewardedAd(rewardedAdUnitId);
            }
            else
            {
                _waitingForLoading = true;
            }
        }
            
        private static void InitializeRewardedAds()
        {
            // Attach callback
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
            MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
            MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
            MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRewardedAdDisplayedEvent;
            MaxSdkCallbacks.OnRewardedAdClickedEvent += OnRewardedAdClickedEvent;
            MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
            MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

            // Load the first RewardedAd
            LoadRewardedAd();
        }

        private static void LoadRewardedAd()
        {
            MaxSdk.LoadRewardedAd( rewardedAdUnitId );
        }

        private static void OnRewardedAdLoadedEvent(string adUnitId)
        {
            // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(rewardedAdUnitId) will now return 'true'
            if (_waitingForLoading)
            {
                _waitingForLoading = false;
                MaxSdk.ShowRewardedAd(rewardedAdUnitId);
            }
        }

        private static void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
        {
            // Rewarded ad failed to load. We recommend re-trying in 3 seconds.
            ReturnCallback(AdsResult.Failed);
        }

        private static void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
        {
            // Rewarded ad failed to display. We recommend loading the next ad
            ReturnCallback(AdsResult.Failed);
        }

        private static void OnRewardedAdDisplayedEvent(string adUnitId) {}

        private static void OnRewardedAdClickedEvent(string adUnitId) {}

        private static void OnRewardedAdDismissedEvent(string adUnitId)
        {
            // Rewarded ad is hidden. Pre-load the next ad
            ReturnCallback(AdsResult.Skipped);
        }

        private static void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
        {
            // Rewarded ad was displayed and user should receive the reward
            ReturnCallback(AdsResult.Finished);
        }

        private static void ReturnCallback(AdsResult result)
        {
            var callback = _onShow;
            _onShow = null;
            _waitingForLoading = false;
            LoadRewardedAd();
            if (callback != null)
            {
                callback(result);
            }
        }
    }
    
    public enum AdsResult
    {
        Finished,
        Skipped,
        Failed
    }
}