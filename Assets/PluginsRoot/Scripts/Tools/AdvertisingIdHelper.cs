﻿using System.Linq;
using UnityEngine;
using Random = System.Random;

namespace PluginsRoot.Tools
{
    public class AdvertisingIdHelper : MonoBehaviour
    {
        public static string DeviceId
        {
            get
            {
                var deviceId = PlayerPrefsStorage.DeviceId;
                if (deviceId != null)
                {
                    return deviceId;
                }

                if (string.IsNullOrEmpty(SystemInfo.deviceUniqueIdentifier))
                {
                    if (string.IsNullOrEmpty(AdvertisingId))
                    {
                        deviceId = RandomString(40);
                    }
                    else
                    {
                        deviceId = AdvertisingId;
                    }
                }
                else
                {
                    deviceId = SystemInfo.deviceUniqueIdentifier;
                }
                PlayerPrefsStorage.DeviceId = deviceId;
                return deviceId;
            }
        }
        
        public static string AdvertisingId { get; private set; }

        void Start()
        {
#if UNITY_EDITOR
            AdvertisingId = "";
#elif UNITY_ANDROID
            Application.RequestAdvertisingIdentifierAsync(AdvertisingIdentifierCallback);
#elif UNITY_IOS
            AdvertisingId = UnityEngine.iOS.Device.advertisingIdentifier;
#endif
        }
        
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "0123456789abcdef";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

#if UNITY_ANDROID
        private void AdvertisingIdentifierCallback(string advertisingId, bool trackingEnabled, string errorMsg)
        {
            if (!trackingEnabled || !string.IsNullOrEmpty(errorMsg))
            {
                AdvertisingId = "";
                return;
            }

            if (!string.IsNullOrEmpty(advertisingId))
            {
                AdvertisingId = advertisingId;
            }
        }
#endif
    }
}