﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace PluginsRoot.Tools
{
    public static class PlayerPrefsStorage
    {
        private const string KeyInstallId = "installId";
        private const string KeyToken = "token";
        
        private const string KeyUserId = "userId";
        private const string KeyUsername = "username";
        private const string KeyDeviceId = "deviceId";
        private const string KeyGoogleId = "googleId";
        private const string KeyAppleId = "appleId";
        private const string KeyFacebookId = "facebookId";
        
        private const string KeyBuild = "build";
        private const string KeyVersion = "version";
        private const string KeyLanguage = "language";

        public static void Initialize()
        {
            Version = 1;
            Build = 1;
            Language = "RU";
        }

        public static int InstallId
        {
            get => GetValue(KeyInstallId, -1);
            set => SetValue(KeyInstallId, value);
        }
        
        public static int Build
        {
            get => GetValue(KeyBuild, -1);
            set => SetValue(KeyBuild, value);
        }
        
        public static int Version
        {
            get => GetValue(KeyVersion, -1);
            set => SetValue(KeyVersion, value);
        }
        
        public static string Language
        {
            get => GetValue(KeyLanguage, null);
            set => SetValue(KeyLanguage, value);
        }
        
        public static string Token
        {
            get => GetValue(KeyToken, null);
            set => SetValue(KeyToken, value);
        }
        
        public static int UserId
        {
            get => GetValue(KeyUserId, -1);
            set => SetValue(KeyUserId, value);
        }
        
        public static string Username
        {
            get => GetValue(KeyUsername, null);
            set => SetValue(KeyUsername, value);
        }
        
        public static string DeviceId
        {
            get => GetValue(KeyDeviceId, null);
            set => SetValue(KeyDeviceId, value);
        }
        
        public static string GoogleId
        {
            get => GetValue(KeyGoogleId, null);
            set => SetValue(KeyGoogleId, value);
        }
        
        public static string AppleId
        {
            get => GetValue(KeyAppleId, null);
            set => SetValue(KeyAppleId, value);
        }
        
        public static string FacebookId
        {
            get => GetValue(KeyFacebookId, null);
            set => SetValue(KeyFacebookId, value);
        }

        private static void SetValue(string key, string value)
        {
            if (value == null)
            {
                PlayerPrefs.DeleteKey(key);
            }
            else
            {
                PlayerPrefs.SetString(key, value);
            }
            PlayerPrefs.Save();
        }

        private static string GetValue(string key, string defaultValue)
        {
            return PlayerPrefs.HasKey(key) ? PlayerPrefs.GetString(key) : defaultValue;
        }
        
        private static void SetValue(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
            PlayerPrefs.Save();
        }

        private static int GetValue(string key, int defaultValue)
        {
            return PlayerPrefs.HasKey(key) ? PlayerPrefs.GetInt(key) : defaultValue;
        }

#if UNITY_EDITOR
        [MenuItem("/PlayerPrefs/Clear All")]
#endif
        public static void ClearAll()
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }
    }
}