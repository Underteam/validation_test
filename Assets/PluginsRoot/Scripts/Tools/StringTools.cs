using UnityEngine;
using System.Text;
using System.Security.Cryptography;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;

namespace PluginsRoot.Tools
{
    //класс с набором статических функция для работы со строками
    public class StringTools
    {
        /*
        public static string GetProperNumeralString_Days(int count)	{return GetProperNumeralString (count,I2Loc.Get("1 день"),I2Loc.Get("2 дня"),I2Loc.Get("5 дней"));}
        public static string GetProperNumeralString_Hours(int count)	{return GetProperNumeralString (count,I2Loc.Get("1 час"),I2Loc.Get("2 часа"),I2Loc.Get("5 часов"));}
        public static string GetProperNumeralString_HoursLeft(int count)	{return GetProperNumeralString (count,I2Loc.Get("Остался 1 час"),I2Loc.Get("Осталось 2 часа"),I2Loc.Get("Осталось 5 часов"));}
        public static string GetProperNumeralString_Minutes(int count)	{return GetProperNumeralString (count,I2Loc.Get("1 минута"),I2Loc.Get("2 минуты"),I2Loc.Get("5 минут"));}
        public static string GetProperNumeralString_MinutesLeft(int count)	{return GetProperNumeralString (count,I2Loc.Get("Осталась 1 минута"),I2Loc.Get("Осталось 2 минуты"),I2Loc.Get("Осталось 5 минут"));}
        public static string GetProperNumeralString_MinutesAlternative(int count)	{return GetProperNumeralString (count,I2Loc.Get("1 минуту"),I2Loc.Get("2 минуты"),I2Loc.Get("5 минут"));}
        public static string GetProperNumeralString_Seconds(int count)	{return GetProperNumeralString (count,I2Loc.Get("1 секунда"),I2Loc.Get("2 секунды"),I2Loc.Get("5 секунд"));}
        public static string GetProperNumeralString_SecondsAlternative(int count)	{return GetProperNumeralString (count,I2Loc.Get("1 секунду"),I2Loc.Get("2 секунды"),I2Loc.Get("5 секунд"));}
        public static string GetProperNumeralString_Gold(int count)	{return GetProperNumeralString (count,I2Loc.Get("1 золото"),I2Loc.Get("2 золота"),I2Loc.Get("5 золота"));}
        public static string GetProperNumeralString_Silver(int count)	{return GetProperNumeralString (count,I2Loc.Get("1 серебро"),I2Loc.Get("2 серебра"),I2Loc.Get("5 серебра"));}
        */

        public static string GetProperNumeralString(int count, string item_1, string item_2_4, string item_5_20)
        {
            int f_count = count % 100;

            if (f_count >= 20)
            {
                f_count = f_count % 10;
            }

            if (f_count == 0 || f_count > 4)
            {
                return item_5_20;
            }
            else if (f_count == 1)
            {
                return item_1;
            }
            else
            {
                return item_2_4;
            }
        }

        public static float Round(float number, int scale)
        {
            int pow = 10;
            for (int i = 1; i < scale; i++)
                pow *= 10;
            float tmp = number * pow;
            return (float) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
        }

        public static string GetColorHash(Color c, bool isNeedTags) // === Получить NGUI TAG для определенного Color
        {
            string val = "";
            if (isNeedTags) val += "[";
            val += GetValueHash(c.r);
            val += GetValueHash(c.g);
            val += GetValueHash(c.b);
            if (isNeedTags) val += "]";
            return val;
        }

        static string GetValueHash(float val)
        {
            string v = ((int) (val * 255f)).ToString("X");
            if (v.Length < 2) v = "0" + v;
            return v;
        }

        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        public static bool HasPlaceholder(string s)
        {
            return Regex.IsMatch(s, "{\\d+}");
        }

        public static string AddApostrophe(string input)
        {
            string result = "";
            int count = 0;
            for (int i = input.Length - 1; i >= 0; --i)
            {
                if (count > 0 && count % 3 == 0)
                {
                    result = "'" + result;
                    ;
                }

                result = input[i] + result;
                count++;
            }

            return result;
        }

        /*
        public static string SecondsToLongCooldownFullWords(int value, int words)
        {
            TimeSpan ts = TimeSpan.FromSeconds ((double)value);
            int cur_w = 0;
            string result = "";
            if (ts.Days >= 1) {
                cur_w++;
                result += string.Format("{0} {1} ", ts.Days, GetProperNumeralString_Days(ts.Days)); 
            }
            if (ts.Hours >= 1 && (cur_w < words)) {
                cur_w++;
                double total_hours = ts.TotalHours - (double)((int)ts.TotalDays * 24);
                int hours = (cur_w == words) ? Mathf.CeilToInt((float)total_hours) : ts.Hours; // Если это последнее слово, округляем вверх
                result += string.Format("{0} {1} ", hours, GetProperNumeralString_Hours(hours)); 
            }
            if ((cur_w < words) && !(ts.Days >= 1 && ts.Hours == 0)) {
                cur_w++;
                double total_minutes = ts.TotalMinutes - (double)((int)ts.TotalHours * 60);
                int minutes = Mathf.CeilToInt((float)total_minutes); // Если это последнее слово, округляем вверх
                result += string.Format("{0} {1}", minutes, GetProperNumeralString_Minutes(minutes)); 
            }
            return result;
        }
        */

        public static string SecondsToCooldown(int value)
        {
            if (value <= 0) return "0" + "сек";
            int min = value / 60;
            int hour = min / 60;
            min -= hour * 60;

            if (hour > 0 || min > 0)
            {
                return ((hour > 0) ? string.Format("{0}" + "ч" + " ", hour) : "") + string.Format("{0}" + "м", min);
            }
            else
            {
                return string.Format("{0}" + "сек", value);
            }
        }

        public static string SecondsToCooldownExpanded(int value)
        {
            if (value <= 0)
            {
                return " ";
                //return "00:00:00";
            }

            int min = value / 60;
            int hour = min / 60;
            int sec = value % 60;
            min -= hour * 60;

            return string.Format("{0:00}:{1:00}:{2:00}", hour, min, sec);
        }

        public static string SecondsToLongCooldown(int value)
        {
            return SecondsToLongCooldown((double) value);
        }

        public static string SecondsToLongCooldown(double value)
        {
            TimeSpan ts = TimeSpan.FromSeconds(value);

            int days = ts.Days;
            int hours = ts.Hours;
            int minutes = ts.Minutes;
            int hours_round = Mathf.CeilToInt((float) ts.TotalHours);
            int minutes_round = Mathf.CeilToInt((float) ts.TotalMinutes);

            string result;

            if (days >= 1)
            {
                return string.Format("{0}{1}", days, "д");
            }

            if (hours >= 1)
            {
                return string.Format("{0}{1}", hours_round, "ч");
            }

            return string.Format("{0}{1}", minutes_round, "м");
        }

        public static string CookNumStringShort(int value)
        {
            if (value < 10000)
            {
                return value.ToString();
            }

            if (value < 1000000)
            {
                // Больше 10К, меньше 1М

                return string.Format("{0}{1}", value / 1000, "К");
//		        if (value < 10000) return string.Format("{0:#.00}{1}", (double)value / 1000.0, "К");
//		        if (value < 100000) return string.Format("{0:#.0}{1}", (double)value / 1000.0, "К");
//		        return string.Format("{0}{1}", (int)value / 1000, "К");
            }


            value /= 1000;
            return string.Format("{0:#.0}{1}", value / 1000.0, "М");
//            if (value < 10000) return string.Format("{0:#.00}{1}", (double)value / 1000.0, "М");
//            else if (value < 100000) return string.Format("{0:#.0}{1}", (double)value / 1000.0, "М");
//            return string.Format("{0}{1}", (int)value / 1000, "М");
        }

        public static string ReplaceBR(string value)
        {
            return value.Replace("[br]", "\n");
        }

        public static string GetPlatformName()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                return "Android";
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                return "iOS";
            }

            //TODO: другой вариант
            return "Android";
        }

        public static Dictionary<string, string> ParseReferrerString(string url)
        {
            //string data = "?google_id=ognev@skytecgames.ru&ref=Referrer is: utm_source=493&utm_medium=link&utm_content=doff&utm_term=allmobile.ru/&utm_campaign=719";
            string data = url;

            string[] items = data.Split('&');
            Dictionary<string, string> result = new Dictionary<string, string>();

            for (int i = 0; i < items.Length; ++i)
            {
                if (items[i].StartsWith("?google_id"))
                {
                    string[] ii = items[i].Split('=');
                    if (ii.Length == 2)
                    {
                        result.Add("google_id", ii[1]);
                    }
                }
                else if (items[i].Contains("utm_source="))
                {
                    string[] ii = items[i].Split(new string[] {"utm_source="}, System.StringSplitOptions.None);
                    if (ii.Length == 2)
                    {
                        result.Add("source", ii[1]);
                    }
                }
                else if (items[i].Contains("utm_medium="))
                {
                    string[] ii = items[i].Split(new string[] {"utm_medium="}, System.StringSplitOptions.None);
                    if (ii.Length == 2)
                    {
                        result.Add("medium", ii[1]);
                    }
                }
                else if (items[i].Contains("utm_content="))
                {
                    string[] ii = items[i].Split(new string[] {"utm_content="}, System.StringSplitOptions.None);
                    if (ii.Length == 2)
                    {
                        result.Add("content", ii[1]);
                    }
                }
                else if (items[i].Contains("utm_term="))
                {
                    string[] ii = items[i].Split(new string[] {"utm_term="}, System.StringSplitOptions.None);
                    if (ii.Length == 2)
                    {
                        result.Add("term", ii[1]);
                    }
                }
                else if (items[i].Contains("utm_campaign="))
                {
                    string[] ii = items[i].Split(new string[] {"utm_campaign="}, System.StringSplitOptions.None);
                    if (ii.Length == 2)
                    {
                        result.Add("campaign", ii[1]);
                    }
                }
            }

            return result;
        }


        public static string CookDateTimeString(DateTime dt)
        {
            string result = dt.Year.ToString() + "-" +
                            dt.Month.ToString().PadLeft(2, '0') + "-" +
                            dt.Day.ToString().PadLeft(2, '0') + " " +
                            dt.Hour.ToString().PadLeft(2, '0') + ":" +
                            dt.Minute.ToString().PadLeft(2, '0') + ":" +
                            dt.Second.ToString().PadLeft(2, '0');
            return result;
        }


        public static string GetNumericVerb(int value, string[] verb)
        {
            if (verb.Length < 3)
            {
                return "";
            }

            value = value % 20;
            if (value == 1)
            {
                return verb[0];
            }
            else if (value <= 4)
            {
                return verb[1];
            }

            return verb[2];
        }
        /*
        public static string ReturnDate(long timestamp)
        {
            return ReturnDate(timestamp,timestamp);
        }
        public static string ReturnDate(long timestamp, long current)
        {
            long div = current - timestamp;
            //DateTime pDate = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(timestamp - div);
            DateTime pDate = DateTime.Now.AddSeconds (-(double)div);

            string result = TwoCharInt(pDate.Day) + " ";
            result += MonthShort (pDate.Month);
            result += ", " + TwoCharInt(pDate.Hour) + ":" + TwoCharInt(pDate.Minute);
            return result;
        }
        */

        public static string TwoCharInt(int val)
        {
            if (val < 10) return "0" + val.ToString();
            return val.ToString();
        }

        public static string GetNumericSentense(int num, string str_1, string str_2, string str_5)
        {
            if (num > 10 && num < 20) return str_5;
            int d = num % 10;
            if (d == 1) return str_1;
            else if (d == 2 || d == 3 || d == 4) return str_2;
            return str_5;
        }

        // ================================================= ПОЛУЧИТЬ ЛОКАЛЬНОЕ ВРЕМЯ ПО TIMESTAMP в формате (сек, мин, ч назад) ==========================
        /*
        public static string GetTimeByTimesatmpsFormat(string time, long current)
        {
            DateTime message = Convert.ToDateTime(time); 	//когда было написано сообщение(сервер)
            System.DateTime dtDateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds((double)current).AddHours(3);
            //Debug.LogError("// current = " + current.ToString() + "// dtDateTime = " + dtDateTime.ToString() + " // message = " + message.ToString());
            return GetTimeByTimesatmpsFormat(System.DateTime.Now.AddSeconds (-(dtDateTime - message).TotalSeconds));
        }
        public static string GetTimeByTimesatmpsFormat(long time, long current)
        {
            long div = current - time;
            return GetTimeByTimesatmpsFormat(System.DateTime.Now.AddSeconds (-(double)div));
        }
        */

        public static string GetObjectHierarhy(GameObject go)
        {
            if (go.transform.parent != null)
            {
                return string.Format("{0}/{1}", GetObjectHierarhy(go.transform.parent.gameObject), go.name);
            }

            return go.name;
        }
    }
}