﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEditor.Callbacks;
#if UNITY_IOS
using System.IO;
using UnityEditor.iOS.Xcode;
#endif

public static class AutoBuilder
{
    // Helper function for getting the command line arguments
    private static string GetArg(string name)
    {
        var args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == name && args.Length > i + 1)
            {
                return args[i + 1];
            }
        }
        return null;
    }

    static string GetProjectName()
    {
        string[] s = Application.dataPath.Split('/');
        return s[s.Length - 2];
    }

    static string[] GetScenePaths()
    {
        string[] scenes = new string[EditorBuildSettings.scenes.Length];

        for (int i = 0; i < scenes.Length; i++)
        {
            scenes[i] = EditorBuildSettings.scenes[i].path;
        }

        return scenes;
    }

    [MenuItem("File/AutoBuilder/iOS")]
    static void PerformiOSBuild()
    {
        EditorApplication.ExecuteMenuItem("Tools/prime[31]/Copy Plugin Files Into Xcode project/Enable for Project");
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.iOS);
        BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/iOS", BuildTarget.iOS, BuildOptions.CompressWithLz4HC);
    }
    
    static void PerformAndroidBuild(bool aab)
    {
        EditorPrefs.SetString("AndroidSdkRoot", GetArg("-androidHome"));
        PlayerSettings.Android.useCustomKeystore = true;
        PlayerSettings.Android.keystoreName = GetArg("-keystoreFile");
        PlayerSettings.Android.keystorePass = GetArg("-keystorePass");
        PlayerSettings.Android.keyaliasName = GetArg("-keyaliasName");
        PlayerSettings.Android.keyaliasPass = GetArg("-keyaliasPass");
        string apkFileName = GetArg("-apkFileName");

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
        EditorUserBuildSettings.buildAppBundle = aab;
        BuildPipeline.BuildPlayer(GetScenePaths(), string.Concat("Builds/", apkFileName, aab ? ".aab" : ".apk"), BuildTarget.Android, BuildOptions.CompressWithLz4HC);
    }
    
    [MenuItem("File/AutoBuilder/iOS-Prod")]
    static void PerformiOSProdBuild()
    {
        PerformiOSBuild();
    }

    [MenuItem("File/AutoBuilder/iOS-Dev")]
    static void PerformiOSDevBuild()
    {
        PerformiOSBuild();
    }

    [MenuItem("File/AutoBuilder/Android-Prod")]
    static void PerformAndroidProdBuild()
    {
        PerformAndroidBuild(true);
    }

    [MenuItem("File/AutoBuilder/Android-Dev")]
    static void PerformAndroidDevBuild()
    {
        PerformAndroidBuild(false);
    }
    
    [PostProcessBuild]
    static void PostProcessBuild(BuildTarget target, string pathToBuildProject)
    {
#if UNITY_IOS
        var proj_path = PBXProject.GetPBXProjectPath(pathToBuildProject);
        var proj = new PBXProject();
        var targetName = proj.TargetGuidByName(PBXProject.GetUnityTargetName());
        proj.ReadFromFile(proj_path);
        
        var mainTarget = proj.TargetGuidByName(targetName);
        proj.SetBuildProperty(mainTarget, "GCC_C_LANGUAGE_STANDARD", "gnu99");
        proj.AddBuildProperty(mainTarget, "GCC_ENABLE_ASM_KEYWORD", "YES");
        proj.AddBuildProperty(mainTarget, "GCC_NO_COMMON_BLOCKS", "NO");
        
        var releaseBuildConfig = proj.BuildConfigByName(mainTarget, "Release");
        var debugBuildConfig = proj.BuildConfigByName(mainTarget, "Debug");
        proj.SetBuildPropertyForConfig(releaseBuildConfig, "GCC_C_LANGUAGE_STANDARD", "gnu99");
        proj.AddBuildPropertyForConfig(releaseBuildConfig, "GCC_ENABLE_ASM_KEYWORD", "YES");
        proj.AddBuildPropertyForConfig(releaseBuildConfig, "GCC_NO_COMMON_BLOCKS", "NO"); 
        proj.SetBuildPropertyForConfig(debugBuildConfig, "GCC_C_LANGUAGE_STANDARD", "gnu99");
        proj.AddBuildPropertyForConfig(debugBuildConfig, "GCC_ENABLE_ASM_KEYWORD", "YES");
        proj.AddBuildPropertyForConfig(debugBuildConfig, "GCC_NO_COMMON_BLOCKS", "NO");
        
        File.WriteAllText(proj_path, proj.WriteToString());
#endif
    }
}